using UnityEngine;

public class Path: MonoBehaviour {
    [SerializeField]
    private RectTransform[] _points;

    public int Length {
        get => _points.Length;
    }

    public Vector2 this[int index] {
        get => _points[index].anchoredPosition;
    }
}